package com.tsadigov.dockertest.service;

import com.tsadigov.dockertest.domain.Student;
import com.tsadigov.dockertest.dto.StudentDTO;

public interface StudentService {

    Student getAll();
    void create(StudentDTO studentDTO);

}
